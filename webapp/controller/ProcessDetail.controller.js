sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/UIComponent",
	"sap/ui/core/routing/History",
	"sap/m/MessageToast",
	"sap/ui/layout/HorizontalLayout",
	"sap/ui/layout/VerticalLayout",
    "n20171121/model/Formatter"
], function(Controller,JSONModel,UIComponent,History,MessageToast,HorizontalLayout,VerticalLayout,Formatter) {
	"use strict";

	return Controller.extend("n20171121.controller.ProcessDetail", {
		oFormatter: Formatter, 
		oSignDialog : null,

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf n20171121.view.Detail
		 */
		onInit: function(){
            this._oRouter = UIComponent.getRouterFor(this);
            this._oPage = this.getView().byId("Detail");
            this._oRouter.getRoute("processDetail").attachPatternMatched(this._onObjectMatched, this);    
            
            this._toggleFooter();
        },

        onBackPress: function(oEvent) {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined){
                window.history.go(-1);
            }else{
	            this._oRouter.navTo("processList", {
	                 diagramId: this._sDiagramId,
	                 query:{
	                 	diagramName: this._sDiagramName
	                 }
	            });
            }
        },
        
        onAcceptPress : function(){
			var oView = this.getView();
			this.oSignDialog = oView.byId("acceptDialog");
			
			this.oSignDialog.open();
        },
        onRejectPress : function(){
			var oView = this.getView();
			this.oSignDialog = oView.byId("rejectDialog");
			
			oView.byId("rejectDialog").setTitle(this._oBundle.getText("rejectTitle"));
			this.oSignDialog.open();
        },
        onSignToAccept : function(){
        	var sText = this.getView().byId("acceptDialogTextarea").getValue();
			if(sText === ""){
				MessageToast.show(this._oBundle.getText("noComment"));
			}else{
				this.oSignDialog.close();
			}
        },
        onSignToReject : function(){
        	var sText = this.getView().byId("rejectnDialogTextarea").getValue();
			if(sText === ""){
				MessageToast.show(this._oBundle.getText("noComment"));
			}else{
				this.oSignDialog.close();
			}
        },
        onCloseDialog : function(oEvent){
        	this.oSignDialog.close();
        },
        _toggleVisibility: function () {
            this._oPage.setShowFooter(!this._oPage.getShowFooter());
        },
        _toggleFooter: function () {
            this._oPage.setFloatingFooter(!this._oPage.getFloatingFooter());
        },

        _onObjectMatched: function (oEvent) {   
            this._sDiagramId = oEvent.getParameter("arguments")["?query"].diagramId;
            this._sDiagramName = oEvent.getParameter("arguments")["?query"].diagramName;         
            var requisitionId = decodeURIComponent(oEvent.getParameter("arguments").requisitionId);
            this._onGetDetail(requisitionId);
            this._onGetProcessStep(requisitionId);
        },
        _onGetDetail:function(sId){
            var apiUrl = "/Flow7/api/fdp/m/"+sId;
        	var oProcessDetail = new JSONModel(apiUrl);
			//oProcessList.setData("/Flow7/dashboard/processing","ProcessList");	
			this.getView().setModel(oProcessDetail,"detail");
			
			var oI18NModel = new sap.ui.model.resource.ResourceModel({ bundleName: "n20171121.i18n.i18n"});
			this.getView().setModel(oI18NModel, "i18n");
			this._oBundle = this.getView().getModel("i18n").getResourceBundle();
        },
        
        _onGetProcessStep:function(sId){
            var apiUrl = "/Flow7/api/dashboard/process/"+sId;
        	var oProcessDetail = new JSONModel(apiUrl);
			//oProcessList.setData("/Flow7/dashboard/processing","ProcessList");	
			this.getView().setModel(oProcessDetail,"processStep");
        }

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf n20171121.view.Detail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf n20171121.view.Detail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf n20171121.view.Detail
		 */
		//	onExit: function() {
		//
		//	}

	});
	
});