sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
    "sap/ui/core/UIComponent",
	"sap/ui/model/Filter",
    "sap/m/MessageToast",
    "n20171121/model/Formatter"
], function(Controller,JSONModel,UIComponent,Filter,MessageToast,Formatter) {
	"use strict";

	return Controller.extend("n20171121.controller.ProcessLists", {
		oFormatter: Formatter, 
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf n20171121.view.Filter
		 */
			onInit: function() {
	            this._onLoadList();
	            this._oRouter = UIComponent.getRouterFor(this);
	            this._oRouter.getRoute("processList").attachPatternMatched(this._handleRouteMatched, this);  
			},

	        _handleRouteMatched: function (oEvent) {            
	            var sDiagramId = decodeURIComponent(oEvent.getParameter("arguments").diagramId);
	            this._filterData(sDiagramId);
				this.getView().byId("processPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
	        },
	        _onLoadList:function(){
				this._oProcessList = new JSONModel("/Flow7/api/dashboard/processing");
				this.getView().setModel(this._oProcessList,"list");
	        },
	    	_filterData:function(sDiagramId){
				var aFilters = [];
	    		var filter = new Filter("DiagramId", sap.ui.model.FilterOperator.EQ, sDiagramId);
	    		aFilters.push(filter);
				// update list binding
				var list = this.getView().byId("processingList");
				var binding = list.getBinding("items");
				binding.filter(aFilters, "Application");
	    	},
	    	
			onListItemPressed: function(oEvent){
				var oObject = oEvent.getSource().getBindingContext("list");
				var oItem = oObject.getModel().getProperty(oObject.getPath());
				console.log(oObject.getPath());
				if(oItem.DiagramId === "FDP_P0"){
	                 this._oRouter.navTo("processDetail", {
	                     requisitionId: oItem.RequisitionId,
	                     query:{
	                     	diagramId:oItem.DiagramId,
	                     	diagramName:oItem.DiagramName
	                     }
	                });
				}else{
					MessageToast.show("... unavalible in POC project....");	
				}
			}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf n20171121.view.Filter
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf n20171121.view.Filter
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf n20171121.view.Filter
		 */
		//	onExit: function() {
		//
		//	}

	});

});