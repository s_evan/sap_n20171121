sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
	"sap/ui/Device",
    "sap/m/MessageToast"
], function(Controller,UIComponent,Device,MessageToast) {
	"use strict";

	return Controller.extend("n20171121.controller.ProcessFolder", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf n20171121.view.Master
		 */
		onInit: function() {
			this._oRouter = UIComponent.getRouterFor(this);
           
			var oFolderList = new sap.ui.model.json.JSONModel("/Flow7/api/dashboard/processing/folder");
			this.getView().setModel(oFolderList);
		},
		onPressGoToList:function(oEvent){
			this._showList(oEvent.getParameter("listItem") || oEvent.getSource());
		},
		_showList : function(oObject) {
			var oItem = oObject.getBindingContext().getProperty("Key");
			this._oRouter.navTo("processList", {
				diagramId : oItem.DiagramId,
                 query:{
                 	diagramName: oItem.DiagramName
                 }
			}, true);
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf n20171121.view.Master
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf n20171121.view.Master
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf n20171121.view.Master
		 */
		//	onExit: function() {
		//
		//	}

	});

});