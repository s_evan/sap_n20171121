sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/UIComponent",
    "n20171121/model/Formatter"
], function(Controller,JSONModel,UIComponent,Formatter) {
	"use strict";

	return Controller.extend("n20171121.controller.HistoryList4Detail", {
		oFormatter: Formatter, 

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf n20171121.view.HistoryList4Detail
		 */
		onInit: function(){
            this._oRouter = UIComponent.getRouterFor(this);
            this._oRouter.getRoute("processDetail").attachPatternMatched(this._onObjectMatched, this);    
        },
        
        _onObjectMatched: function (oEvent) {   
            this._sDiagramId = oEvent.getParameter("arguments")["?query"].diagramId;
            this._sDiagramName = oEvent.getParameter("arguments")["?query"].diagramName;         
            var requisitionId = decodeURIComponent(oEvent.getParameter("arguments").requisitionId);
            this._onGetHistory(requisitionId);
        },
        
        _onGetHistory:function(sId){
            //var apiUrl = "/Flow7/api/fdp/s/35712496-9527-4e4d-b56d-81542ce1ea0c";//+sId;
            var apiUrl = "/Flow7/api/fdp/s/"+sId;
        	var oProcessDetail = new JSONModel(apiUrl);
			//oProcessList.setData("/Flow7/dashboard/processing","ProcessList");	
			this.getView().setModel(oProcessDetail,"historyList");
        }

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf n20171121.view.ProcessStep4Detail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf n20171121.view.ProcessStep4Detail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf n20171121.view.ProcessStep4Detail
		 */
		//	onExit: function() {
		//
		//	}

	});

});
